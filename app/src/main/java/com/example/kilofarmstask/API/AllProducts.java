package com.example.kilofarmstask.API;

import java.util.List;

public class AllProducts {

    private int response;
    private List<SingleProduct> data;

    public int getResponse() {
        return response;
    }

    public void setResponse(int response) {
        this.response = response;
    }

    public List<SingleProduct> getData() {
        return data;
    }

    public void setData(List<SingleProduct> data) {
        this.data = data;
    }
}
