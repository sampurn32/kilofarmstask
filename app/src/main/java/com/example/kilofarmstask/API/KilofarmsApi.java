package com.example.kilofarmstask.API;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class KilofarmsApi {

    public static ApiInterface service = null;

    public static ApiInterface postService(){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://cc2pruxs38.execute-api.us-east-1.amazonaws.com/staging/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service = retrofit.create(ApiInterface.class);
        return service;
    }

    public static ApiInterface getService(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://6j57eve9a1.execute-api.us-east-1.amazonaws.com/staging/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(ApiInterface.class);
        return service;
    }

    public interface ApiInterface {

        @POST("{category}")
        Call<PostResponse> postData(@Path("category") String category, @Body PostBody body);

        @GET("{category}?item=all")
        Call<AllProducts> getAllProducts(@Path("category") String category, @Query("userId") String userId );

        @GET("{category}")
        Call<SingleProduct> getSingleProduct(@Path("category") String category, @Query("item") String item, @Query("userId") String id);
    }
}
