package com.example.kilofarmstask;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.google.android.material.textfield.TextInputLayout;

public class Dashboard extends AppCompatActivity {
    private TextInputLayout password;
    private TextInputLayout phone;
    private Button Login;
    private ImageView Logo;
    private TextView Slogan;

    AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        password = findViewById(R.id.password);
        phone = findViewById(R.id.phone);
        Login = findViewById(R.id.GO);
        Logo = findViewById(R.id.logoImage);
        Slogan = findViewById(R.id.Welcome);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        awesomeValidation.addValidation(this,R.id.phone
                ,"[5-9]{1}[0-9]{9}$",R.string.invalid_phone);
        awesomeValidation.addValidation(this,R.id.password,
                "^" +
                        "(?=.*[@#$%^&+=])" +     // at least 1 special character
                        "(?=\\S+$)" +            // no white spaces
                        ".{4,}" +                // at least 4 characters
                        "$",R.string.invalid_password);


        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (awesomeValidation.validate()){
                    Toast.makeText(getApplicationContext(),
                            "Login Validated ...",Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent( Dashboard.this,Product.class);
                    Pair[] pairs = new Pair[2];
                    pairs[0] = new Pair <View,String>(Logo, "logo_image");
                    pairs[1] = new Pair <View,String>(Slogan, "slogan");
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(Dashboard.this,pairs);
                        startActivity(intent,options.toBundle());
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(),
                            "Invalid Login",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}