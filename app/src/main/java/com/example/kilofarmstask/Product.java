package com.example.kilofarmstask;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kilofarmstask.API.KilofarmsApi;
import com.example.kilofarmstask.API.PostBody;
import com.example.kilofarmstask.API.PostResponse;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class Product<product_name> extends AppCompatActivity {

    ImageView Logo;
    TextView Slogan;
    static String user_id="106118085";
    String product_nam;
    //TextInputEditText product_name = findViewById(R.id.product_name);
    String product_category;

    String unit = "KG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Logo = findViewById(R.id.imageView2);
        Slogan = findViewById(R.id.textView2);
        Button submit = findViewById(R.id.submit);
        final Button list_product = findViewById(R.id.list_products);
        Spinner spinner = (Spinner) findViewById(R.id.product_category);
//        product_category= spinner.getSelectedItem().toString();
         product_category="Fruits";
         product_nam="Apple";
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.product_types, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"Product Registered",Toast.LENGTH_SHORT).show();
                final Call<PostResponse> data = KilofarmsApi.postService().postData(user_id, new PostBody(product_nam,user_id,unit, product_category));    //category string and body of PostBody type
                data.enqueue(new Callback<PostResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<PostResponse> call, @NonNull Response<PostResponse> response) {
                        if(response.isSuccessful()){
                            PostResponse postResponse = response.body();
                            if(postResponse.getResponse() == 200){
                                Toast.makeText(getApplicationContext(),postResponse.getMessage(),Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<PostResponse> call,@NonNull Throwable t) {
                        //Add appropriate message
                    }
                });
            }
        });

        list_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Product.this,Product_list.class);
                //startActivity(intent);
                                Pair[] pairs = new Pair[2];
                                pairs[0] = new Pair <View,String>(Logo, "logo_image");
                                pairs[1] = new Pair <View,String>(Slogan, "slogan");
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                                    ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(Product.this,pairs);
                                    startActivity(intent,options.toBundle());
                                }
            }
        });

    }
}