package com.example.kilofarmstask.API;

public class PostBody {

    private String skuName;
    private String user_id;
    private String skuUnit;
    private String skuCategory;

    public PostBody(String skuName, String user_id, String skuUnit, String skuCategory) {
        this.skuName = skuName;
        this.user_id = user_id;
        this.skuUnit = skuUnit;
        this.skuCategory = skuCategory;
    }
}
